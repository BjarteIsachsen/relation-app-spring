package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectDB {
    private Connection connection;
    public ConnectDB(){
        connection = connect();
    }

    public Connection getConnection() {
        return connection;
    }

    private Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:resources/Contact.db";
        Connection connect = null;
        try {
            connect = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return connect;
    }
}
