package no.noroff.relationapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RelationAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(RelationAppApplication.class, args);
	}

}
